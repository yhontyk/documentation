---
title: "Celebrating our successes"
type: blog
date: "2021-10-07"
author: Nikolai Kondrashov
---
Last Friday we accepted another facet of our new, mostly-remote reality, and
held an online party to celebrate and acknowledge the successes we had in the
past year. Normally, we would've met several times a year at conferences and
meetings in Brno, Brussels and elsewhere, and while another online meeting
didn't really have the same flavor, it was still nice to get together for
something else but work proper.

We had plenty of causes:

* we moved most of our code, configuration and documentation to
  [public repositories](https://gitlab.com/cki-project) on gitlab.com (before
  we had some of them on Red Hat's GitLab instance for security reasons);
* we moved all our
  [pipelines](https://gitlab.com/redhat/red-hat-ci-tools/kernel/)
  to gitlab.com as well;
* we played a major part in moving Red Hat's Linux Kernel development workflow
  to gitlab.com;
* we supported the launch of
  [CentOS Stream](https://gitlab.com/redhat/centos-stream/src/kernel/centos-stream-9)
  development workflow;
* we opened our [Data Warehouse](https://datawarehouse.cki-project.org/) to
  the public;
* and a ton of other smaller things!

We mostly just hung out and chatted about the sorts of things you normally
would in a get-together like this: weather, COVID-19, movies, climate change,
electric cars, 3d printing, and so on. Well, OK, some of those might have been
nerdier than normal.

While listening and occasionally joining the chat, I finally followed through
with my plan to design a physical representation of our project's logo:

<!-- markdownlint-disable no-inline-html -->
<p style="text-align: center">
    <img src="logo.svg" style="width: 20em; margin: 2em;">
</p>
<!-- markdownlint-restore -->

In case you didn't know, apart from being an acronym for "Continuous Kernel
Integration", "CKI" can also be read as a contraction of "cookie", symbolizing
the test results we're giving freely to the community (like cookies). Thus the
cookie shapes in the logo, and my idea to model the logo as a plate of cookies
for 3D printing:

<!-- markdownlint-disable no-inline-html -->
<p style="text-align: center">
    <img src="3d_print_1.jpeg"
         style="margin: 2em; border-radius: 2em; width: 90%">
</p>
<!-- markdownlint-restore -->

It finished printing right after our party was over. On Monday I had a
refined version, ready for a key ring:

<!-- markdownlint-disable no-inline-html -->
<p style="text-align: center">
    <img src="3d_print_2.jpeg"
         style="margin: 2em; border-radius: 2em; width: 90%">
</p>
<!-- markdownlint-restore -->

You'll be able to get one of those on the very next in-person conference we'll
be able to attend. If you want one now, give us a shout and we'll send you
a few.

Now we gotta work hard some more, so we could hold another celebration, and I
could perhaps design cookie cutters to make real CKI cookies!
