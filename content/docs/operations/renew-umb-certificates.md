---
title: Renew expired UMB certificates
linkTitle: Renew UMB certificates
description: How to renew the certificates used to authenticate against shared services, such as UMB.
---

## Problem

There are 2 service accounts used to access UMB:

1. `cki-amqp-bridge`: Read only permission.

1. `cki-umb-messenger`: Write permission on the following topics:

   - `VirtualTopic.eng.cki.ready_for_test`
   - `VirtualTopic.eng.ci.cki.#`

These service accounts use certificates to authenticate.

## Steps

1. The certificates are automatically renewed every year via the [Dogtag
   certificate manager]. No manual steps are necessary!

## Useful Links

- [Accessing Shared Services]
- [UMB Getting Started]
- [How to: Request UMB Certificates]
- [INC1643166]: Service Account creation ticket.
- [RITM0854859]: UMB authorization ticket.

[Dogtag certificate manager]: https://gitlab.com/cki-project/cki-tools/-/blob/main/README.refresh_dogtag_certificates.md
[Accessing Shared Services]: https://source.redhat.com/groups/public/identity-access-management/identity__access_management_wiki/ldap_service_accounts__certificates_for_accessing_shared_services
[How to: Request UMB Certificates]: https://source.redhat.com/groups/public/enterprise-services-platform/it_platform_wiki/how_to_request_a_messaging_client_certificate_from_rhcs
[UMB Getting Started]: https://source.redhat.com/groups/public/enterprise-services-platform/it_platform_wiki/unified_message_bus_umb_getting_started
[INC1643166]: https://redhat.service-now.com/surl.do?n=INC1643166
[RITM0854859]: https://redhat.service-now.com/surl.do?n=RITM0854859
