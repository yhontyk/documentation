---
title: Celebrating success
description: How to celebrate a success from IRC
---

## Problem

A successful event includes anything that moves the CKI project ahead,
especially things that make life better for CKI team members or our customers.
It includes but is not limited to:

* Software releases
* Bug fixes
* Performance improvements
* Someone makes a huge mistake and then fixes it
* People say nice (or mostly nice) things about the CKI project
* We save someone some time
* We find a kernel bug or a test bug
* New functionality is added
* Anything that makes you happy

You may find yourself asking: *Is this worthy of a mention to the success
bot?*

If you ask yourself this question, the answer is **yes**. As Alfred Lloyd
Tennyson once said:

> Tis better to have noted success and lost, then never to have noted success
> at all.

The quote was something like that.

## Steps

1. Join `#kernelci` on Red Hat IRC

1. Tell the [CKI IRC bot] (`cki_bot`) about it via its `success` command:

   ```text
   cki_bot: success 🎉 By testing bpf-next, found a gcc dwarf generation bug 🎉
   ```

1. The line you gave to the bot should now appear on the [success page] in the
   CEE GitLab instance.

   ![🎉 Success 🎉](happy-dance.gif)

[CKI IRC bot]: https://gitlab.com/cki-project/irc-bot
[success page]: https://gitlab.cee.redhat.com/snippets/1429
