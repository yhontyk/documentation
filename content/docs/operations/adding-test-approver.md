---
title: Adding a new approver to kpet-db
linkTitle: Kernel tests approver
description: >
    How to manage the list of people that can approve merge requests in the
    kpet-db repository
---

## Problem

In the [com/kpet-db] repository

- the correct reviewers for most merge requests are outside the CKI team
- the number of seats is limited on [com/cki-project]

Therefore, the following setup is used:

- the [cki-kernel-tests-reviewers LDAP group] contains everybody that should
  have approval rights on the [com/kpet-db] repository
- this group is linked via SAML to the [cki-kernel-tests-reviewers GitLab group]
- the GitLab group is imported into a custom merge request approval rule for
  the [com/kpet-db] repository, configured in [cee/deployment-all]

## Steps

1. As an owner of the [cki-kernel-tests-reviewers LDAP group], click on `Edit`
   and modify the member list as needed.

1. If a member was removed, manually remove the member from the
   [cki-kernel-tests-reviewers GitLab group] as well.

1. If a member was added, ask them to relogin via [GitLab SSO] so the new
   permissions get applied.

[com/kpet-db]: https://gitlab.com/cki-project/kpet-db
[com/cki-project]: https://gitlab.com/cki-project
[cee/deployment-all]: https://gitlab.cee.redhat.com/cki-project/deployment-all/-/blob/main/gitlab-repo-config/config.yml

[cki-kernel-tests-reviewers LDAP group]: https://rover.redhat.com/groups/group/cki-kernel-tests-reviewers
[cki-kernel-tests-reviewers GitLab group]: https://gitlab.com/redhat/red-hat-ci-tools/kernel/cki-kernel-tests-reviewers

[GitLab SSO]: https://red.ht/GitLabSSO
