---
title: Updating pipeline images
description: |
    How to update the container images used in the pipeline
---

The pipeline uses container images with date-based tags.

To release a container image, create and push a new tag via

```shell
git tag 20191031.0
git push origin 20191031.0
```

It is also possible to create tags in GitLab's interface via *Repository* →
*Tags* → *New tag*.

All tags must be in the `YYYYMMDD.release` format. For example the first
release on October 31, 2019 would be: `20191031.1`. If a new release is needed
on the same day, increment the release number by one to make `20191031.2`.

Once the CI pipelines are finished, the container images should be visible in
GitLab's container repository. Verify that by clicking *Packages* and then
*Container Registry*.

To deploy new container images in the pipeline, create a merge request in
[pipeline-definition]:

- change the `{image_tag}` and `{builder_image_tag}` variables in
  `pipeline/variables.yml` and `.gitlab-ci.yml`
- add a comment to the merge request that contains: `@cki-ci-bot test`
- wait for the pipelines to finish
- verify everything is correct
- request a review of the merge request

[pipeline-definition]: https://gitlab.com/cki-project/pipeline-definition
