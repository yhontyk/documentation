---
title: Updating cross-compilers
linkTitle: Updating cross-compilers
description: How to update the cross-compiler packages
---

## Problem

RHEL 8 and 9 have no official cross-compilers, nor are they available in EPEL.
We do have native compilation and multi-arch container images for RHEL 8 and 9,
but we also need the cross compilers for the kernel binary itself.

The `tools team` created their own packages and we're using them for building
the appropriate `builder-rhelXX` and `builder-streamXX` images.

Right now, the packages are stored in the CKI S3 bucket (via [lookaside]), so the
container image templates can download them.

The problem is that checking for updates in the cross-compiler tools, packaging
them, testing them and, then, updating the RPMs in the S3 bucket is a manual
process.

The `tools team`, actually, **Nick Clifton**, [has automated the first part]
(detecting updates and building the new packages), but we need to test them and
change the RPMs at [lookaside].

## Steps

1. Every time the `tools team` detects updates, they'll build the new packages
   in Brew and send an email to the CKI mailing list.
   The email will show the commands to download the updated packages. Something
   like this:

   <!-- markdownlint-disable line-length -->
   ```plain
   Subject: New RHEL-8 gcc and glibc cross rpms available

   Body:
   Hi,

   Another day, another set of new rpms:

   New cross gcc rpms (8.4.1-1.el8) can be downloaded with:     brew download-task 35331760
   New cross glibc rpms (2.28-151.el8) can be downloaded with:  brew download-task 35333362

   Cheers
    Nick
   ```
   <!-- markdownlint-enable line-length -->

1. Download the new package with the provided brew command
(`brew download-task ${task-id}`).

1. Create a merge request at [lookaside], changing the old rpms for
   the new ones.

1. Create a merge request at the [containers] repository, changing the RPMs' URLs
   for the updated packages, at the appropriate container template. Use the
   RPMs' URLs from the new [lookaside] branch. Create a **merge request** to
   see if the images build fine.

1. If the images builds fine, trigger the CKI CI bot in a dummy MR in the
   [pipeline-definition] repository to run a pipeline for the right stream,
   using the image from that merge request.

   ```plain
   @cki-ci-bot test [cki/rhel8][builder_image_tag=mr-272]
   ```

   > **NOTE:** `mr-272` would be (in this example) the merge request code.

1. If the pipeline runs fine, merge both merge requests, the one from `lookaside`
   and the one from `containers`.

## In case it fails

In case the pipeline (or the image build) fails, contact **Nick Clifton** to
see how to fix it.

[lookaside]: https://gitlab.com/cki-project/lookaside
[has automated the first part]: https://gitlab.cee.redhat.com/nickc/cross-tools-for-the-kernel
[pipeline-definition]: https://gitlab.com/cki-project/pipeline-definition/
[containers]: https://gitlab.com/cki-project/containers
