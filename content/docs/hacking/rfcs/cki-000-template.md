---
title: "CKI-000: Request for Comments (RFC) template"
linkTitle: CKI-000
description: |
    Template to use for new CKI Requests for Comments
layout: rfc
author: Name
mr: 1
---

## Abstract

This document serves as a template for new Request for Comment (RFC) documents.
The process behind them is described in [CKI-001]. This abstract should contain
a one paragraph explanation of the RFC.

## Motivation

Why is this RFC needed?

## Approach

What is the solution to the need? Which steps are needed to get there?

## Benefits

What is the expected outcome?

## Drawbacks

Why should we not do this?

## Alternatives

Which alternative approaches are available instead of the proposed solution?
Why is the proposed solution substantially better?

[CKI-001]: cki-001-cki-feedback-mechanism.md
<!-- vi: set spell spelllang=en: -->
