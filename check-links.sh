#!/bin/bash

set -euo pipefail

if ! command -v muffet &> /dev/null; then
    alias muffet="podman run --network=host registry.gitlab.com/cki-project/cki-tools/cki-tools:production muffet"
fi

hugo serve -p 1313 &
trap 'trap - EXIT && kill $!' EXIT
sleep 3

ignores=()
while IFS='' read -r ignore; do
    ignores+=(--exclude "${ignore}")
done < <(cat check-links-*.txt)

muffet http://localhost:1313/ \
    --buffer-size=8192 \
    --max-connections=16 \
    --timeout 60 \
    --ignore-fragments \
    "${ignores[@]}"

# if no dangling links were found, shut down normally
kill %1
trap - EXIT
